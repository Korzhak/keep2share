from src.downloader import download
from settings import *
from src.api_processing import K2SAPI
import sys
import re
import os

links = []
FILE_NOT_FOUND = "File not found"
FILE_NAME_IN_ARCHIVE = 1
FILE_EXTENDS = 2

if PATH[-1] != "/":
    PATH += "/"

if PATH_TO_EXTRACT[-1] != "/":
    PATH_TO_EXTRACT += "/"


def run():
    try:
        source = open(FILE_WITH_LINKS)
        for line in source.readlines():
            links.append(line.strip())

    except:
        print('Reading is not completed.')

    else:
        source.close()
        while "" in links:
            links.remove("")

        print("Number of files: " + str(links.__len__()))

        # GETTING VALID LINKS FOR DOWNLOAD
        obj = K2SAPI(USERNAME, PASSWORD, links)

        # DOWNOLOADING FILES
        for file_name in obj.valid_links:
            if not obj.valid_links[file_name] == FILE_NOT_FOUND:
                download(obj.valid_links[file_name], path=PATH + file_name,
                         file_name=file_name)
            else:
                print(FILE_NOT_FOUND)

        # EXTRACTING FILES
        if EXTRACT_RAR_FILES:
            stream = sys.stderr
            stream.write("EXTRACTING FILES\n")
            stream.flush()
            regex = r"(\w+).part\d+.(\w+)"
            extracted_files = []

            def unrar(rar_dir, output_dir):
                os.system("unrar e " + rar_dir + " " + output_dir)

            for file_name in obj.valid_links:
                # split filename
                sf = re.search(regex, file_name)
                if sf.group(FILE_EXTENDS) == "rar" \
                        and sf.group(FILE_NAME_IN_ARCHIVE) not in extracted_files:
                    unrar(PATH + file_name, PATH_TO_EXTRACT)
                    extracted_files.append(sf.group(FILE_NAME_IN_ARCHIVE))
