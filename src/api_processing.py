import requests

URL = "http://keep2share.cc/api/v2"
POST = "POST"
GET = "GET"
AUTH_TOKEN = 'auth_token'
FILE_NAME = -1
FILE_ID = -2


class K2SAPI:
    def __init__(self, username, passwd, links=[]):
        self.username = username
        self.passwd = passwd
        self.raw_links = links
        self.auth_token = self.login()[AUTH_TOKEN]
        self.files_id = self.parse_links()
        self.valid_links = self.get_valid_links()

    def login(self):
        url = f"{URL}/login"
        params = {
            "username": self.username,
            "password": self.passwd,
        }
        return requests.post(url, json=params).json()

    def parse_links(self):
        parsed_links = {}
        for link in self.raw_links:
            list_link = link.split("/")
            parsed_links[
                list_link[FILE_NAME]
            ] = list_link[FILE_ID]
        return parsed_links

    def get_url(self, file_id):
        url = f"{URL}/geturl"
        params = {
            "auth_token": self.auth_token,
            "file_id": file_id
        }
        try:
            valid_links = requests.post(url, json=params).json()['url']
        except:
            return "File not found"
        else:
            return valid_links

    def get_valid_links(self):
        valid_links = {}
        for name_file in self.files_id:
            valid_links[name_file] = self.get_url(self.files_id[name_file])
        return valid_links
